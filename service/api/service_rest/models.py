from django.db import models


class Technician(models.Model):
    first_name = models.CharField(null=True, max_length=200)
    last_name = models.CharField(null=True, max_length=200)
    employee_id = models.CharField(null=True, max_length=200)

    def __str__(self):
        return f"{self.first_name} {self.last_name} ({self.employee_id})"


class AutomobileVO(models.Model):
    vin = models.CharField(null=True, max_length=200)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return f"VIN: {self.vin} - {'Sold' if self.sold else 'Available'}"


class Appointment(models.Model):
    date_time = models.DateTimeField(auto_now_add=True)
    reason = models.CharField(max_length=255)
    status = models.CharField(max_length=100)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(Technician, on_delete=models.CASCADE)

    def __str__(self):
        return (
            f"Appointment on {self.date_time} for {self.reason} - Status: {self.status}"
        )
