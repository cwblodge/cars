from django.shortcuts import render
from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.utils.dateparse import parse_datetime
from .encoders import (
    AutomobileVODetailEncoder,
    TechnicianDetailEncoder,
    TechnicianListEncoder,
    AppointmentDetailEncoder,
    AppointmentListEncoder,
)


@require_http_methods(["GET", "POST"])
def api_list_technician(request):

    if request.method == "GET":
        technician_id = request.GET.get("id")
        if technician_id is not None:
            try:
                technician = Technician.objects.get(id=technician_id)
                return JsonResponse(
                    technician, encoder=TechnicianDetailEncoder, safe=False
                )
            except Technician.DoesNotExist:
                return JsonResponse({"message": "Technician not found"}, status=404)
        else:
            technicians = Technician.objects.all()
            return JsonResponse(
                {"technicians": list(technicians)},
                encoder=TechnicianListEncoder,
                safe=False,
            )

    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(
                first_name=content.get("first_name"),
                last_name=content.get("last_name"),
                employee_id=content.get("employee_id"),
                id=content.get("id"),
            )
            return JsonResponse(technician, encoder=TechnicianDetailEncoder, safe=False)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)


@require_http_methods(["DELETE", "GET"])
def api_show_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            response_data = {
                "first_name": technician.first_name,
                "last_name": technician.last_name,
                "employee_id": technician.employee_id,
                "id": technician.id,
            }
            return JsonResponse(response_data)
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician not found"}, status=404)

    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                {"message": "Technician deleted successfully"}, status=204
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician not found"}, status=404)


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointment_id = request.GET.get("id")
        if appointment_id is not None:
            try:
                appointment = Appointment.objects.get(id=appointment_id)
                response_data = {
                    "date_time": appointment.date_time,
                    "reason": appointment.reason,
                    "status": appointment.status,
                    "vin": appointment.vin,
                    "customer": appointment.customer,
                    "technician_id": appointment.technician.id,
                }
                return JsonResponse(response_data)
            except Appointment.DoesNotExist:
                return JsonResponse({"message": "Appointment not found"}, status=404)
        else:
            appointments = Appointment.objects.all()
            data = [
                {
                    "id": app.id,
                    "date_time": app.date_time,
                    "reason": app.reason,
                    "status": app.status,
                    "vin": app.vin,
                    "customer": app.customer,
                    "technician_id": app.technician.id,
                }
                for app in appointments
            ]
            return JsonResponse({"appointments": data}, safe=False)

    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(id=content["technician_id"])
            date_time = parse_datetime(content["date_time"])
            if date_time is None:
                return JsonResponse({"message": "Invalid date_time format"}, status=400)
            appointment = Appointment.objects.create(
                date_time=content["date_time"],
                reason=content["reason"],
                status=content["status"],
                vin=content["vin"],
                customer=content["customer"],
                technician=technician,
            )
            response_data = {
                "id": appointment.id,
                "date_time": appointment.date_time,
                "reason": appointment.reason,
                "status": appointment.status,
                "vin": appointment.vin,
                "customer": appointment.customer,
                "technician_id": appointment.technician.id,
            }
            return JsonResponse(response_data, status=201)
        except KeyError:
            return JsonResponse({"message": "Missing required fields"}, status=400)
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician not found"}, status=404)
        except json.JSONDecodeError:
            return JsonResponse({"message": "Invalid JSON"}, status=400)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=500)


@require_http_methods(["POST", "DELETE", "PUT"])
def api_appointment(request, pk=None):
    if request.method == "POST":
        try:
            content = json.loads(request.body)
            date_time = parse_datetime(content["date_time"])
            if date_time is None:
                return JsonResponse({"message": "Invalid date_time format"}, status=400)
            technician = Technician.objects.get(id=content["technician_id"])
            appointment = Appointment.objects.create(
                date_time=date_time,
                reason=content["reason"],
                status=content["status"],
                vin=content["vin"],
                customer=content["customer"],
                technician=technician,
            )
            return JsonResponse(
                {
                    "id": appointment.id,
                    "date_time": appointment.date_time,
                    "reason": appointment.reason,
                    "status": appointment.status,
                    "vin": appointment.vin,
                    "customer": appointment.customer,
                    "technician_id": appointment.technician.id,
                },
                status=201,
            )
        except KeyError:
            return JsonResponse({"message": "Missing required fields"}, status=400)
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician not found"}, status=404)
        except json.JSONDecodeError:
            return JsonResponse({"message": "Invalid JSON"}, status=400)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=500)

    elif request.method == "DELETE":
        if pk:
            try:
                appointment = Appointment.objects.get(id=pk)
                appointment.delete()
                return JsonResponse(
                    {"message": "Appointment deleted successfully"}, status=204
                )
            except Appointment.DoesNotExist:
                return JsonResponse({"message": "Appointment not found"}, status=404)
        else:
            return JsonResponse({"message": "No appointment ID provided"}, status=400)

    elif request.method == "PUT":
        if pk:
            try:
                content = json.loads(request.body)
                status = content.get("status")
                if status not in ["canceled", "finished"]:
                    return JsonResponse(
                        {"message": "Invalid status provided"}, status=400
                    )

                appointment = Appointment.objects.get(id=pk)
                appointment.status = status
                appointment.save()

                response_data = {
                    "id": appointment.id,
                    "date_time": appointment.date_time,
                    "reason": appointment.reason,
                    "status": appointment.status,
                    "vin": appointment.vin,
                    "customer": appointment.customer,
                    "technician_id": appointment.technician.id,
                }
                return JsonResponse(response_data)
            except Appointment.DoesNotExist:
                return JsonResponse({"message": "Appointment not found"}, status=404)
            except json.JSONDecodeError:
                return JsonResponse({"message": "Invalid JSON"}, status=400)
            except KeyError:
                return JsonResponse(
                    {"message": "Missing 'status' field in the request"}, status=400
                )
            except Exception as e:
                return JsonResponse({"message": str(e)}, status=500)
        else:
            return JsonResponse({"message": "No appointment ID provided"}, status=400)
