from django.urls import path
from .api_views import (
    api_list_technician,
    api_show_technician,
    api_list_appointments,
    api_appointment,
)

urlpatterns = [
    path("technicians/", api_list_technician, name="api_create_technician"),
    path("technicians/<int:pk>/", api_show_technician, name="api_delete_technician"),
    path("appointments/", api_list_appointments, name="api_create_appointment"),
    path("appointments/<int:pk>/", api_appointment, name="api_delete_appointment"),
    path(
        "appointments/<int:pk>/cancel/", api_appointment, name="api_cancel_appointment"
    ),
    path(
        "appointments/<int:pk>/finish/", api_appointment, name="api_finish_appointment"
    ),
]
