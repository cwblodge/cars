# Generated by Django 4.0.3 on 2024-05-08 18:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("service_rest", "0002_servicehistory"),
    ]

    operations = [
        migrations.DeleteModel(
            name="ServiceHistory",
        ),
    ]
