from common.json import ModelEncoder
from .models import Salesperson, Customer, Sale, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id", "id"]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ["price", "date", "salesperson", "customer", "automobile", "id"]
    encoders = {
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
        "automobile": AutomobileVOEncoder(),
    }

    def encode_salesperson(self, obj):
        return f"{obj.salesperson.first_name} {obj.salesperson.last_name}"

    def encode_customer(self, obj):
        return f"{obj.customer.first_name} {obj.customer.last_name}"

    def encode_automobile(self, obj):
        return obj.automobile.vin

    def default(self, obj):
        data = super().default(obj)
        data["salesperson_name"] = self.encode_salesperson(obj)
        data["customer_name"] = self.encode_customer(obj)
        data["automobile_vin"] = self.encode_automobile(obj)
        return data
