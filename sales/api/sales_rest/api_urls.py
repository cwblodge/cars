from django.urls import path
from .views import (
    salespeople,
    salesperson_detail,
    customers,
    customer_detail,
    sales,
    sale_detail,
    sales_by_salesperson,
)

urlpatterns = [
    path("salespeople/", salespeople, name="salespeople"),
    path("salespeople/<int:id>/", salesperson_detail, name="salesperson_detail"),
    path("customers/", customers, name="customers"),
    path("customers/<int:id>/", customer_detail, name="customer_detail"),
    path("sales/", sales, name="sales"),
    path("sales/detail/<int:id>/", sale_detail, name="sale_detail"),
    path("sales/<int:id>/", sales_by_salesperson, name="sales_by_salesperson"),
]
