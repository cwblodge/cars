from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Salesperson, Customer, Sale, AutomobileVO
import json
from sales_rest.encoders import SalespersonEncoder, CustomerEncoder, SaleEncoder


@require_http_methods(["GET", "POST", "DELETE"])
def salesperson_detail(request, id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(pk=id)
            return JsonResponse(salesperson, encoder=SalespersonEncoder, safe=False)
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson not found"}, status=404)
    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(pk=id)
            salesperson.delete()
            return JsonResponse({"message": "Salesperson deleted"})
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson not found"}, status=404)
    elif request.method == "POST":
        data = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.get(pk=id)
            for key, value in data.items():
                setattr(salesperson, key, value)
            salesperson.save()
            return JsonResponse(salesperson, encoder=SalespersonEncoder, safe=False)
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson not found"}, status=404)


@require_http_methods(["GET"])
def sales_by_salesperson(request, id):
    salesperson_id = id
    if not salesperson_id:
        return JsonResponse({"message": "Salesperson ID is required"}, status=400)
    try:
        salesperson = Salesperson.objects.get(pk=salesperson_id)
        sales = Sale.objects.filter(salesperson=salesperson)
        sales_data = [format_sale(sale) for sale in sales]
        return JsonResponse(sales_data, safe=False)
    except Salesperson.DoesNotExist:
        return JsonResponse({"message": "Salesperson not found"}, status=404)
    except Exception as e:
        return JsonResponse({"message": str(e)}, status=500)


def format_sale(sale):
    return {
        "id": sale.id,
        "salesperson_name": f"{sale.salesperson.first_name} {sale.salesperson.last_name}",
        "salesperson_id": sale.salesperson.id,
        "customer_name": f"{sale.customer.first_name} {sale.customer.last_name}",
        "customer_id": sale.customer.id,
        "automobile_vin": sale.automobile.vin,
        "price": str(sale.price),
    }


@require_http_methods(["GET", "POST"])
def salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            list(salespeople.values(*SalespersonEncoder.properties)),
            encoder=SalespersonEncoder,
            safe=False,
        )
    elif request.method == "POST":
        data = json.loads(request.body)
        salesperson = Salesperson.objects.create(**data)
        return JsonResponse(
            {
                prop: getattr(salesperson, prop)
                for prop in SalespersonEncoder.properties
            },
            encoder=SalespersonEncoder,
            status=201,
        )


@require_http_methods(["GET", "POST", "DELETE"])
def customer_detail(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(pk=id)
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer not found"}, status=404)
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(pk=id)
            customer.delete()
            return JsonResponse({"message": "Customer deleted"})
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer not found"}, status=404)
    elif request.method == "POST":
        data = json.loads(request.body)
        try:
            customer = Customer.objects.get(pk=id)
            for key, value in data.items():
                setattr(customer, key, value)
            customer.save()
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer not found"}, status=404)


@require_http_methods(["GET", "POST"])
def customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        print(customers[0].id)
        print(customers[0].first_name)
        return JsonResponse(
            list(customers.values(*CustomerEncoder.properties)),
            encoder=CustomerEncoder,
            safe=False,
        )
    elif request.method == "POST":
        data = json.loads(request.body)
        customer = Customer.objects.create(**data)
        return JsonResponse(
            {prop: getattr(customer, prop) for prop in CustomerEncoder.properties},
            encoder=CustomerEncoder,
            status=201,
        )


@require_http_methods(["GET", "POST", "DELETE"])
def sale_detail(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(pk=id)
            return JsonResponse(sale, encoder=SaleEncoder, safe=False)
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Sale not found"}, status=404)
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(pk=id)
            sale.delete()
            return JsonResponse({"message": "Sale deleted"})
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Sale not found"}, status=404)
    elif request.method == "POST":
        data = json.loads(request.body)
        try:
            sale = Sale.objects.get(pk=id)
            for key, value in data.items():
                setattr(sale, key, value)
            sale.save()
            return JsonResponse(sale, encoder=SaleEncoder, safe=False)
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Sale not found"}, status=404)


@require_http_methods(["GET", "POST"])
def sales(request):
    if request.method == "GET":
        salesperson_id = request.GET.get("salesperson_id")
        if salesperson_id:
            try:
                salesperson_id = int(salesperson_id)
                sales = Sale.objects.filter(
                    salesperson_id=salesperson_id
                ).select_related("salesperson", "customer", "automobile")
            except ValueError:
                return JsonResponse(
                    {"error": "Salesperson ID must be an integer."}, status=400
                )
        else:
            sales = Sale.objects.all().select_related(
                "salesperson", "customer", "automobile"
            )

        sales_data = [
            {
                "id": sale.id,
                "salesperson_name": f"{sale.salesperson.first_name} {sale.salesperson.last_name}",
                "salesperson_id": sale.salesperson.id,
                "customer_name": f"{sale.customer.first_name} {sale.customer.last_name}",
                "customer_id": sale.customer.id,
                "automobile_vin": sale.automobile.vin,
                "price": sale.price,
            }
            for sale in sales
        ]
        return JsonResponse(sales_data, safe=False)

    elif request.method == "POST":
        try:
            data = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse({"error": "Invalid JSON."}, status=400)

        automobile = data.get("automobile")
        automobile_vin = automobile["vin"]
        salesperson = data.get("salesperson")
        salesperson_id = salesperson["id"]
        customer = data.get("customer")
        customer_id = customer["id"]
        price = data.get("price")

        if not all([automobile_vin, salesperson, customer, price]):
            return JsonResponse(
                {
                    "error": "Missing necessary field(s): automobile, salesperson, customer, price."
                },
                status=400,
            )

        try:
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            salesperson = Salesperson.objects.get(id=salesperson_id)
            customer = Customer.objects.get(id=customer_id)
            sale = Sale.objects.create(
                automobile=automobile,
                salesperson=salesperson,
                customer=customer,
                price=price,
            )
            return JsonResponse(
                {
                    "id": sale.id,
                    "salesperson_name": f"{salesperson.first_name} {salesperson.last_name}",
                    "customer_name": f"{customer.first_name} {customer.last_name}",
                    "automobile_vin": sale.automobile.vin,
                    "price": sale.price,
                },
                status=201,
            )
        except Exception as e:
            print(e)
            return JsonResponse({"error": str(e)}, status=500)
