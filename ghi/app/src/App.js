import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AddSalesperson from './AddSalesperson';
import ListSalespeople from './ListSalespeople';
import AddCustomer from './AddCustomer';
import ListCustomers from './ListCustomers';
import RecordSale from './RecordSale';
import ListSales from './ListSales';
import SalespersonHistory from './SalespersonHistory';
import Nav from './Nav';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import './styles/global.css'; 
import ManufacturerList from './ManufacturerList'; 
import CreateManufacturer from './CreateManufacturer';
import VehicleModels from './VehicleModels';
import CreateVehicleModel from './CreateVehicleModel';
import AddAutomobile from './AddAutomobile';
import AutomobilesList from './AutomobilesList';
import MainPage from './MainPage';




function App() {
  return (
      <><Nav /><div className="container">
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="/technicians" element={<TechnicianList />} />
        <Route path="/technicians/new" element={<TechnicianForm />} />
        <Route path="/appointments/new" element={<AppointmentForm />} />
        <Route path="/appointments" element={<AppointmentList />} />
        <Route path="/service-history" element={<ServiceHistory />} />
        <Route path="/add-salesperson" element={<AddSalesperson />} />
        <Route path="/list-salespeople" element={<ListSalespeople />} />
        <Route path="/add-customer" element={<AddCustomer />} />
        <Route path="/list-customers" element={<ListCustomers />} />
        <Route path="/record-sale" element={<RecordSale />} />
        <Route path="/list-sales" element={<ListSales />} />
        <Route path="/salesperson-history" element={<SalespersonHistory />} />
        <Route path="/manufacturers" element={<ManufacturerList />} />
        <Route path="/create-manufacturer" element={<CreateManufacturer />} />
        <Route path="/models" element={<VehicleModels />} />
        <Route path="/create-model" element={<CreateVehicleModel />} />
        <Route path="/automobiles" element={<AutomobilesList />} />
        <Route path="/add-automobile" element={<AddAutomobile />} />
      </Routes>
    </div></>
  );
}

export default App;  
