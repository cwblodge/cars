import React, { useEffect, useState } from 'react';

function ListSalespeople() {
  const [salespeople, setSalespeople] = useState([]);

  useEffect(() => {
    async function fetchSalespeople() {
      try {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (!response.ok) {
          throw new Error('Failed to fetch salespeople');
        }
        const data = await response.json();
        setSalespeople(data);
      } catch (error) {
        console.error('Error fetching salespeople:', error);
      }
    }
    fetchSalespeople();
  }, []);

  const handleDelete = async (id) => {
    try {
      const response = await fetch(`http://localhost:8090/api/salespeople/${id}`, {
        method: 'DELETE',
      });
      if (!response.ok) {
        throw new Error('Failed to delete salesperson');
      }
      setSalespeople(salespeople.filter(salesperson => salesperson.id !== id));
    } catch (error) {
      console.error('Error deleting salesperson:', error);
    }
  };

  return (
    <div>
      <h2>Salespeople List</h2>
      <div className="table">
        <div className="table-header">
          <div className="header__item">First Name</div>
          <div className="header__item">Last Name</div>
          <div className="header__item">Employee ID</div>
          <div className="header__item">Actions</div> 
        </div>
        <div className="table-content">
          {salespeople.map((salesperson, index) => (
            <div className={`table-row ${index % 2 === 0 ? 'shaded' : ''}`} key={salesperson.employee_id}>
              <div className="table-data">{salesperson.first_name}</div>
              <div className="table-data">{salesperson.last_name}</div>
              <div className="table-data">{salesperson.employee_id}</div>
              <div className="table-data">
                <button onClick={() => handleDelete(salesperson.id)} style={{ backgroundColor: '#ff4747', color: 'white', border: 'none', borderRadius: '4px', padding: '5px 10px', cursor: 'pointer' }}>
                  Delete
                </button>
              </div> 
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default ListSalespeople;
