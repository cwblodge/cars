import React, { useState, useEffect } from 'react';


function TechniciansList() {
 const [technicians, setTechnicians] = useState([]);
 const [error, setError] = useState("");


 useEffect(() => {
   const fetchTechnicians = async () => {
     try {
       const response = await fetch('http://localhost:8080/api/technicians/');
       if (!response.ok) {
         throw new Error('Failed to fetch technicians');
       }
       const data = await response.json();
       setTechnicians(data.technicians);
     } catch (err) {
       setError("Unable to fetch data. Please try again later.");
       console.error('Fetch error:', err);
     }
   };


   fetchTechnicians();
 }, []);


 const handleDelete = async (e, technicianId) => {
   e.preventDefault();
   const url = `http://localhost:8080/api/technicians/${technicianId}`;
   try {
     const response = await fetch(url, { method: 'DELETE' });
     if (!response.ok) {
       throw new Error('Failed to delete the technician');
     }
     setTechnicians(prevTechnicians => prevTechnicians.filter(technician => technician.id !== technicianId));
   } catch (error) {
     setError("Failed to delete technician. Please try again later.");
     console.error("Delete operation failed:", error);
   }
 };


 return (
  <div>
    <h2 style={{ color: '#E63946' }}>Technicians</h2>
    <table style={{ width: '100%', borderCollapse: 'collapse' }}>
      <thead>
        <tr>
          <th style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>First Name</th>
          <th style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>Last Name</th>
          <th style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>Employee ID</th>
          <th style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>Actions</th>
        </tr>
      </thead>
      <tbody>
        {technicians.map(technician => (
          <tr key={technician.id}>
            <td style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>{technician.first_name}</td>
            <td style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>{technician.last_name}</td>
            <td style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>{technician.employee_id}</td>
            <td style={{ border: '1px solid #ccc', padding: '8px' }}>
              <button onClick={(event) => handleDelete(event, technician.id)} style={{ backgroundColor: '#ff4747', color: 'white', border: 'none', borderRadius: '4px', padding: '5px 10px', cursor: 'pointer' }}>
                Delete
              </button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);

}

export default TechniciansList;