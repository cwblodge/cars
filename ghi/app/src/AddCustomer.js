import React, { useState } from 'react';

function AddCustomer() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [address, setAddress] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();
    const customerData = {
      first_name: firstName,
      last_name: lastName,
      address,
      phone_number: phoneNumber
    };

    try {
      const response = await fetch('http://localhost:8090/api/customers/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(customerData)
      });

      if (!response.ok) {
        const errorData = await response.json(); 
        throw new Error(errorData.message || 'Failed to add customer'); 
      }

      const result = await response.json();
      console.log('Customer added:', result);
      alert('Customer added successfully!');
      setFirstName('');
      setLastName('');
      setAddress('');
      setPhoneNumber('');
    } catch (error) {
      console.error('Error:', error);
      alert(error.message); 
    }
  };

  return (
    <div style={{ padding: 20, maxWidth: 400, margin: "auto", color: '#E63946' }}>
      <h2>Add a Customer</h2>
      <form onSubmit={handleSubmit} style={{ display: 'table', width: '100%' }}>
        <div style={{ display: 'table-row' }}>
          <div style={{ display: 'table-cell', padding: 10 }}>
            <label style={{ color: 'black' }}>First name:</label>
          </div>
          <div style={{ display: 'table-cell', padding: 10 }}>
            <input
              type="text"
              value={firstName}
              onChange={e => setFirstName(e.target.value)}
              placeholder="First name..."
              required
              style={{ width: '100%', padding: '8px', border: '1px solid #ccc', borderRadius: '4px', color: 'black' }}
            />
          </div>
        </div>
        <div style={{ display: 'table-row' }}>
          <div style={{ display: 'table-cell', padding: 10 }}>
            <label style={{ color: 'black' }}>Last name:</label>
          </div>
          <div style={{ display: 'table-cell', padding: 10 }}>
            <input
              type="text"
              value={lastName}
              onChange={e => setLastName(e.target.value)}
              placeholder="Last name..."
              required
              style={{ width: '100%', padding: '8px', border: '1px solid #ccc', borderRadius: '4px', color: 'black' }}
            />
          </div>
        </div>
        <div style={{ display: 'table-row' }}>
          <div style={{ display: 'table-cell', padding: 10 }}>
            <label style={{ color: 'black' }}>Address:</label>
          </div>
          <div style={{ display: 'table-cell', padding: 10 }}>
            <input
              type="text"
              value={address}
              onChange={e => setAddress(e.target.value)}
              placeholder="Address..."
              required
              style={{ width: '100%', padding: '8px', border: '1px solid #ccc', borderRadius: '4px', color: 'black' }}
            />
          </div>
        </div>
        <div style={{ display: 'table-row' }}>
          <div style={{ display: 'table-cell', padding: 10 }}>
            <label style={{ color: 'black' }}>Phone number:</label>
          </div>
          <div style={{ display: 'table-cell', padding: 10 }}>
            <input
              type="text"
              value={phoneNumber}
              onChange={e => setPhoneNumber(e.target.value)}
              placeholder="Phone number..."
              required
              style={{ width: '100%', padding: '8px', border: '1px solid #ccc', borderRadius: '4px', color: 'black' }}
            />
          </div>
        </div>
        <div style={{ display: 'table-row' }}>
          <div style={{ display: 'table-cell', padding: 10, colspan: 2 }}>
            <button type="submit" style={{
              width: '100%',
              backgroundColor: '#4CAF50', 
              color: 'white', 
              border: 'none', 
              borderRadius: '4px', 
              padding: '10px', 
              cursor: 'pointer'
            }}>
              Create
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}

export default AddCustomer;
