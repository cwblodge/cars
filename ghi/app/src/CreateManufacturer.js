import React, { useState } from 'react';

function CreateManufacturer() {
  const [name, setName] = useState('');
  const [submitting, setSubmitting] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();
    setSubmitting(true);
    try {
      const response = await fetch('http://localhost:8100/api/manufacturers/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ name })
      });
      if (response.ok) {
        const data = await response.json();
        console.log('Manufacturer created:', data);
        setName(''); 
      } else {
        throw new Error('Failed to create manufacturer');
      }
    } catch (error) {
      console.error('Failed to create manufacturer', error);
    } finally {
      setSubmitting(false);
    }
  };

  return (
    <div style={{ padding: '20px 0', backgroundColor: '#f5f5f5', minHeight: '100vh' }}>
      <form onSubmit={handleSubmit} style={{
          width: '100%', 
          maxWidth: '500px', 
          margin: '0 auto', 
          backgroundColor: 'white', 
          padding: '20px', 
          boxShadow: '0 4px 8px rgba(0,0,0,0.1)', 
          borderRadius: '8px',
          marginTop: '20px'  
        }}>
        <h1 style={{ marginBottom: '20px', textAlign: 'center' }}>Create Manufacturer</h1>
        <div style={{ marginBottom: '20px' }}>
          <label style={{ display: 'block', color: 'black', marginBottom: '10px' }}>Name:</label>
          <input
            type="text"
            value={name}
            onChange={e => setName(e.target.value)}
            required
            style={{ width: '100%', padding: '10px', border: '1px solid #ccc', borderRadius: '4px' }}
          />
        </div>
        <button type="submit" disabled={submitting} style={{
            width: '100%', 
            padding: '10px', 
            backgroundColor: '#4CAF50', 
            color: 'white', 
            border: 'none', 
            borderRadius: '4px', 
            cursor: 'pointer', 
            fontSize: '16px'
          }}>
          {submitting ? 'Creating...' : 'Create'}
        </button>
      </form>
    </div>
  );
}


export default CreateManufacturer;
