import React, { useEffect, useState } from 'react';

function ListSales() {
  const [sales, setSales] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    async function fetchSales() {
      try {
        const response = await fetch('http://localhost:8090/api/sales');
        if (!response.ok) {
          throw new Error('Failed to fetch sales');
        }
        const data = await response.json();
        setSales(data);
      } catch (error) {
        setError('Failed to load data: ' + error.message);
      } finally {
        setIsLoading(false);
      }
    }
    fetchSales();
  }, []);

  const handleDelete = async (id) => {
    try {
      const response = await fetch(`http://localhost:8090/api/sales/detail/${id}`, {
        method: 'DELETE',
      });
      if (!response.ok) {
        throw new Error('Failed to delete sale');
      }
      setSales(sales.filter(sale => sale.id !== id));
    } catch (error) {
      console.error('Error deleting sale:', error);
    }
  };

  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;

  return (
    <div style={{ padding: '20px', maxWidth: '1200px', margin: 'auto', color: '#E63946'}}> 
      <h2>List of Sales</h2>
      <div style={{ display: 'grid', gridTemplateColumns: 'repeat(5, 1fr)', gap: '10px', marginBottom: '20px' }}>
        <div style={{ color: 'black' }}><strong>Salesperson Name</strong></div> 
        <div style={{ color: 'black' }}><strong>Customer Name</strong></div> 
        <div style={{ color: 'black' }}><strong>VIN</strong></div> 
        <div style={{ color: 'black' }}><strong>Price</strong></div> 
        <div style={{ color: 'black' }}><strong>Actions</strong></div> 
      </div>
      {sales.map((sale, index) => (
        <div key={sale.id} style={{ 
            display: 'grid', 
            gridTemplateColumns: 'repeat(5, 1fr)', 
            gap: '10px', 
            backgroundColor: index % 2 === 0 ? '#f0f0f0' : 'transparent', 
            padding: '10px',
            color: 'black'  
          }}>
          <div>{sale.salesperson_name}</div>
          <div>{sale.customer_name}</div>
          <div>{sale.automobile_vin}</div>
          <div>${sale.price}</div>
          <div>
            <button onClick={() => handleDelete(sale.id)} style={{
              backgroundColor: '#ff4747', 
              color: 'white', 
              border: 'none', 
              borderRadius: '4px', 
              padding: '5px 10px', 
              cursor: 'pointer'
            }}>
              Delete
            </button>
          </div>
        </div>
      ))}
    </div>
  );
}
export default ListSales;

