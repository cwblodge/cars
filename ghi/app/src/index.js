import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css';
import { BrowserRouter } from "react-router-dom";

const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadData() {
  try {

    const techniciansResponse = await fetch('http://localhost:8080/api/technicians/');
    const appointmentsResponse = await fetch('http://localhost:8080/api/appointments/');

    if (!techniciansResponse.ok || !appointmentsResponse.ok) {
      throw new Error(`Failed to fetch data: ${techniciansResponse.statusText}, ${appointmentsResponse.statusText}`);
    }

    const techniciansData = await techniciansResponse.json();
    const appointmentsData = await appointmentsResponse.json();


    root.render(
      <React.StrictMode>
        <BrowserRouter>
          <App
            technicians={techniciansData.technicians}
            appointments={appointmentsData.appointments}
          />
        </BrowserRouter>
      </React.StrictMode>
    );

  } catch (error) {
    console.error("Error loading initial data:", error);

    root.render(
      <div>
        <h1>Failed to load initial data</h1>
        <p>Error: {error.message}</p>
      </div>
    );
  }
}


loadData();
