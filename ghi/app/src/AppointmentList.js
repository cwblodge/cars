import React, { useState, useEffect } from 'react';

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);

  useEffect(() => {
    fetchAppointments();
  }, []);

  const fetchAppointments = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/');
    if (response.ok) {
      const data = await response.json();
      const appointmentsWithTechnicians = await Promise.all(data.appointments.map(async app => {
        const techResponse = await fetch(`http://localhost:8080/api/technicians/${app.technician_id}`);
        const techData = techResponse.ok ? await techResponse.json() : { first_name: 'Unknown', last_name: 'Technician' };
        return {
          ...app,
          technician_name: `${techData.first_name} ${techData.last_name}`,
          isVIP: await checkVIPStatus(app.vin),
        };
      }));
      setAppointments(appointmentsWithTechnicians);
    }
  };

  const checkVIPStatus = async (vin) => {
    try {
      const response = await fetch(`http://localhost:8100/api/automobiles/${vin}`);
      if (response.ok) {
        const { sold } = await response.json();
        return sold;
      }
    } catch (error) {
      console.error('Error checking VIP status:', error);
    }
    return false;
  };

  const handleStatusChange = async (e, appointmentId, newStatus) => {
    e.preventDefault();
    const action = newStatus === 'finished' ? 'finish' : 'cancel';
    const statusToSend = newStatus === 'finished' ? 'finished' : 'canceled';
    const url = `http://localhost:8080/api/appointments/${appointmentId}/${action}/`;
    const response = await fetch(url, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ status: statusToSend }),
    });

    if (response.ok) {
      const updatedAppointments = appointments.map(app =>
        app.id === appointmentId ? {...app, status: statusToSend} : app
      );
      setAppointments(updatedAppointments);
    } else {
      const errorText = await response.text();
      console.error('Failed to update the status:', errorText);
      alert('Failed to update the status. Please try again.');
    }
  };

  return (
    <div className="container">
      <h1>Scheduled Appointments</h1>
      <table className="table table-striped">
        <thead className="thead-dark">
          <tr>
            <th>Date/Time</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
            <th>VIP</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((app) => (
            <tr key={app.id}>
              <td>{new Date(app.date_time).toLocaleString()}</td>
              <td>{app.customer}</td>
              <td>{app.vin}</td>
              <td>{app.technician_name}</td>
              <td>{app.reason}</td>
              <td>{app.status}</td>
              <td>{app.isVIP ? 'Yes' : 'No'}</td>
              <td>
                <button className="btn" style={{ backgroundColor: '#28a745', color: 'white', marginRight: '8px' }} onClick={(event) => handleStatusChange(event, app.id, 'finished')}>Finish</button>
                <button className="btn" style={{ backgroundColor: '#dc3545', color: 'white' }} onClick={(event) => handleStatusChange(event, app.id, 'cancelled')}>Cancel</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentList;
