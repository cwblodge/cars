import React, { useState, useEffect } from 'react';


function AutomobilesList() {
    const [automobiles, setAutomobiles] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8100/api/automobiles/')
            .then(response => response.json())
            .then(data => {
                setAutomobiles(data.autos);
            })
            .catch(error => console.error('Error fetching automobiles:', error));
    }, []);

    return (
        <div> 
            <h1 style={{ textAlign: 'center', color: '#E63946' }}>Automobiles in Inventory</h1>
            <table style={{ width: '100%', borderCollapse: 'collapse' }}>
                <thead>
                    <tr>
                        <th style={{ color: 'red', borderBottom: '2px solid #ddd' }}>VIN</th>
                        <th style={{ color: 'orange', borderBottom: '2px solid #ddd' }}>Color</th>
                        <th style={{ color: 'yellow', borderBottom: '2px solid #ddd' }}>Year</th>
                        <th style={{ color: 'green', borderBottom: '2px solid #ddd' }}>Model</th>
                        <th style={{ color: 'blue', borderBottom: '2px solid #ddd' }}>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map(auto => (
                        <tr key={auto.vin}>
                            <td style={{ color: 'black' }}>{auto.vin}</td>
                            <td style={{ color: 'black' }}>{auto.color}</td>
                            <td style={{ color: 'black' }}>{auto.year}</td>
                            <td style={{ color: 'black' }}>{auto.model.name}</td>
                            <td style={{ color: 'black' }}>{auto.sold ? 'Yes' : 'No'}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
    
}

export default AutomobilesList;
