import React from 'react';
import { NavLink } from 'react-router-dom';
import './styles/nav.css';

function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark navbar-mario-kart">
            <div className="container-fluid">
                <NavLink className="navbar-brand" to="/">CarCar</NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <NavItem to="/" text="Home" />
                        <NavItem to="/technicians" text="List Technicians" />
                        <NavItem to="/technicians/new" text="Add Technician" />
                        <NavItem to="/appointments/new" text="Create an Appointment" />
                        <NavItem to="/appointments" text="List Appointments" />
                        <NavItem to="/service-history" text="Service History" />
                        <NavItem to="/add-salesperson" text="Add Salesperson" />
                        <NavItem to="/list-salespeople" text="List Salespeople" />
                        <NavItem to="/add-customer" text="Add Customer" />
                        <NavItem to="/list-customers" text="List Customers" />
                        <NavItem to="/record-sale" text="Record New Sale" />
                        <NavItem to="/list-sales" text="List Sales" />
                        <NavItem to="/salesperson-history" text="Salesperson History" />
                        <NavItem to="/manufacturers" text="List Manufacturers" />
                        <NavItem to="/create-manufacturer" text="Create Manufacturer" />
                        <NavItem to="/models" text="List Vehicle Models" />
                        <NavItem to="/create-model" text="Create Vehicle Model" />
                        <NavItem to="/automobiles" text="List Automobiles" />
                        <NavItem to="/add-automobile" text="Add Automobile" />
                    </ul>
                </div>
            </div>
        </nav>
    );
}

const NavItem = ({ to, text }) => (
    <li className="nav-item">
        <NavLink 
            className="nav-link" 
            to={to} 
            style={({ isActive }) => isActive ? {
                fontWeight: 'bold', 
                color: '#FFD700', 
                backgroundColor: '#FF6347', 
                borderRadius: '5px',
                padding: '10px 15px',
                margin: '0 10px',
                boxShadow: '0 4px 8px rgba(0, 0, 0, 0.2)', 
                transition: 'background-color 0.3s, color 0.3s, box-shadow 0.3s'
            } : {
                color: '#FFFFFF', 
                backgroundColor: '#007BFF', 
                borderRadius: '5px',
                padding: '10px 15px',
                margin: '0 10px',
                boxShadow: '0 2px 4px rgba(0, 0, 0, 0.1)', 
                transition: 'background-color 0.3s, color 0.3s, box-shadow 0.3s'
            }} 
            end 
        >
            {text}
        </NavLink>
    </li>
);

export default Nav;


// const NavItem = ({ to, text }) => (
//     <li className="nav-item">
//         <NavLink className="nav-link" to={to} style={({ isActive }) => isActive ? { fontWeight: 'normal', color: '#025fbc', backgroundColor: '#f0f0f0', borderRadius: '5px', padding: '10px 15px', margin: '0 10px', boxShadow: '0 2px 4px rgba(0, 0, 0, 0.1)', transition: 'background-color 0.3s, color 0.3s, box-shadow 0.3s' } : undefined} end >
//             {text}
//         </NavLink>
//     </li>
// );

// export default Nav;
