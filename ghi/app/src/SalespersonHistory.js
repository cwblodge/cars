import React, { useState, useEffect } from 'react';

function SalespersonHistory() {
  const [salespeople, setSalespeople] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState('');
  const [sales, setSales] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (!response.ok) {
          throw new Error('Failed to fetch salespeople');
        }
        const data = await response.json();
        setSalespeople(data);
        setError('');
      } catch (error) {
        setError('Failed to load data: ' + error.message);
      } finally {
        setIsLoading(false);
      }
    };
    fetchData();
  }, []);

  useEffect(() => {
    if (selectedSalesperson) {
      setIsLoading(true);
      const fetchSales = async () => {
        try {
          const response = await fetch(`http://localhost:8090/api/sales/${selectedSalesperson}`);
          if (!response.ok) throw new Error('Failed to fetch sales');
          const data = await response.json();
          setSales(data);
        } catch (error) {
          setError(`Error fetching sales: ${error.message}`);
        } finally {
          setIsLoading(false);
        }
      };
      fetchSales();
    } else {
      setSales([]); 
    }
  }, [selectedSalesperson]);

return (
    <div>
      <h2 style={{ color: '#E63946' }}>Salesperson's Sales History</h2>
      {isLoading && <p>Loading...</p>}
      {error && <p style={{ color: 'red' }}>{error}</p>}
      <select 
        value={selectedSalesperson} 
        onChange={e => setSelectedSalesperson(e.target.value)}
        style={{ color: 'black', padding: '8px', margin: '10px 0' }}
      >
        <option value="">Select a Salesperson</option>
        {salespeople.map(person => (
          <option key={person.id} value={person.id} style={{ color: 'black' }}>
            {person.first_name} {person.last_name}
          </option>
        ))}
      </select>
      <table style={{ width: '100%', borderCollapse: 'collapse' }}>
        <thead>
          <tr>
            <th style={{ color: 'black', borderBottom: '1px solid #ccc', padding: '8px' }}>Salesperson</th>
            <th style={{ color: 'black', borderBottom: '1px solid #ccc', padding: '8px' }}>Customer</th>
            <th style={{ color: 'black', borderBottom: '1px solid #ccc', padding: '8px' }}>VIN</th>
            <th style={{ color: 'black', borderBottom: '1px solid #ccc', padding: '8px' }}>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map(sale => {
            return (
              <tr key={sale.id}>
                <td style={{ color: 'black', padding: '8px' }}>{sale.salesperson_name}</td>
                <td style={{ color: 'black', padding: '8px' }}>{sale.customer_name}</td>
                <td style={{ color: 'black', padding: '8px' }}>{sale.automobile_vin}</td>
                <td style={{ color: 'black', padding: '8px' }}>${sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default SalespersonHistory;


