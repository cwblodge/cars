import React, { useState, useEffect } from 'react';

function ServiceHistory() {
  const [history, setHistory] = useState([]);

  useEffect(() => {
    fetchServiceHistory();
  }, []);

  const fetchServiceHistory = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/appointments/');
      if (!response.ok) {
        throw new Error('Failed to fetch appointments data');
      }
      const result = await response.json();
      const fetchedAppointments = result.appointments;

      if (Array.isArray(fetchedAppointments)) {
        const historyWithTechnicians = await Promise.all(fetchedAppointments.map(async (entry) => {
          const isVIP = entry.vin ? await checkVIPStatus(entry.vin) : false;
          const technicianData = entry.technician_id ? await fetchTechnician(entry.technician_id) : { first_name: 'Unknown', last_name: 'Technician' };

          return {
            ...entry,
            isVIP,
            technician: technicianData,
          };
        }));

        setHistory(historyWithTechnicians);
      } else {
        throw new Error('Fetched data is not an array');
      }
    } catch (error) {
      console.error('Error fetching appointments data:', error);
    }
  };

  const checkVIPStatus = async (vin) => {
    const response = await fetch(`http://localhost:8100/api/automobiles/${vin}`);
    if (response.ok) {
      const { sold } = await response.json();
      return sold;
    }
    return false;
  };

  const fetchTechnician = async (technicianId) => {
    const response = await fetch(`http://localhost:8080/api/technicians/${technicianId}`);
    if (response.ok) {
      return response.json();
    }
    return { first_name: 'Unknown', last_name: 'Technician' };
  };

  return (
    <div className="container">
      <h1>Service History</h1>
      <table className="table table-bordered">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {history.map((item, index) => (
            <tr key={index}>
              <td>{item.vin}</td>
              <td>{item.isVIP ? 'Yes' : 'No'}</td>
              <td>{item.customer}</td>
              <td>{new Date(item.date_time).toLocaleDateString()}</td>
              <td>{new Date(item.date_time).toLocaleTimeString()}</td>
              <td>{item.technician ? `${item.technician.first_name} ${item.technician.last_name}` : 'No technician assigned'}</td>
              <td>{item.reason}</td>
              <td>{item.status}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistory;
