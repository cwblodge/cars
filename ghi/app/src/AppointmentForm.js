import React, { useState, useEffect } from 'react';

function AppointmentForm() {
  const [technicians, setTechnicians] = useState([])

  const [formData, setFormData] = useState({
    date_time: '',
    reason: '',
    status: '',
    vin: '',
    customer: '',
    technician: '',
  })

  const fetchTechnicians = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }

  useEffect(() => {
    fetchTechnicians();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const formDataToSubmit = {
      ...formData,
      technician_id: formData.technician,
    };

    const url = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formDataToSubmit),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setFormData({
          date_time: '',
          reason: '',
          status: '',
          vin: '',
          customer: '',
          technician: '',
        });

        alert("Appointment created successfully!");
      } else {
        const errorData = await response.json();
        alert(`Failed to create appointment: ${errorData.message}`);
      }
    } catch (error) {
      console.error('Fetch error:', error);
      alert('Failed to create appointment due to a network error.');
    }
  }


  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4" style={{ color: 'black' }}>
          <h1>Create a new appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-group mb-3">
              <label htmlFor="date_time">Date and Time</label>
              <input onChange={handleFormChange} required type="datetime-local" name="date_time" id="date_time" className="form-control" value={formData.date_time} />
            </div>
  
            <div className="form-group mb-3">
              <label htmlFor="reason">Reason</label>
              <input onChange={handleFormChange} required type="text" name="reason" id="reason" className="form-control" value={formData.reason} />
            </div>
  
            <div className="form-group mb-3">
              <label htmlFor="status">Status</label>
              <input onChange={handleFormChange} required type="text" name="status" id="status" className="form-control" value={formData.status} />
            </div>
  
            <div className="form-group mb-3">
              <label htmlFor="vin">Vehicle Identification Number (VIN)</label>
              <input onChange={handleFormChange} required type="text" name="vin" id="vin" className="form-control" value={formData.vin} />
            </div>
  
            <div className="form-group mb-3">
              <label htmlFor="customer">Customer</label>
              <input onChange={handleFormChange} required type="text" name="customer" id="customer" className="form-control" value={formData.customer} />
            </div>
  
            <div className="form-group mb-3">
              <label htmlFor="technician">Technician</label>
              <select onChange={handleFormChange} required name="technician" id="technician" className="form-select" value={formData.technician}>
                <option value="">Select a Technician</option>
                {technicians.map(technician => (
                  <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                ))}
              </select>
            </div>
  
            <button type="submit" className="btn" style={{ backgroundColor: '#28a745', color: 'white' }}>Create Appointment</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default AppointmentForm;
