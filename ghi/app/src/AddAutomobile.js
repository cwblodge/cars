import React, { useState, useEffect } from 'react';

function AddAutomobile() {
    const [models, setModels] = useState([]);
    const [formData, setFormData] = useState({
        vin: '',
        color: '',
        year: '',
        modelId: '',
        sold: false
    });
    const [message, setMessage] = useState('');

    useEffect(() => {
        fetch('http://localhost:8100/api/models/')
            .then(response => response.json())
            .then(data => setModels(data.models))
            .catch(error => console.error('Error fetching models:', error));
    }, []);

    const handleChange = (event) => {
        const { name, value, type, checked } = event.target;
        setFormData(prevFormData => ({
            ...prevFormData,
            [name]: type === 'checkbox' ? checked : value
        }));
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        fetch('http://localhost:8100/api/automobiles/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                vin: formData.vin,
                color: formData.color,
                year: parseInt(formData.year),
                model_id: formData.modelId,
                sold: formData.sold
            })
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            setMessage('Automobile added successfully.');
            setFormData({
                vin: '',
                color: '',
                year: '',
                modelId: '',
                sold: false
            });
        })
        .catch(error => {
            console.error('Error creating automobile:', error);
            setMessage('Failed to add automobile. Check console for errors.');
        });
    };

    return (
        <div style={{ 
            padding: '20px', 
            maxWidth: '600px', 
            margin: 'auto', 
            backgroundColor: '#FFFFFF', 
            borderRadius: '8px', 
            boxShadow: '0 4px 8px rgba(0,0,0,0.3)' 
        }}>
            <h1 style={{ color: '#E63946' }}>Add an Automobile to Inventory</h1> 
            <form onSubmit={handleSubmit} style={{ 
                display: 'flex', 
                flexDirection: 'column', 
                gap: '10px'
            }}>
                <label style={{ color: '#1D3557' }}> 
                    VIN:
                    <input type="text" name="vin" value={formData.vin} onChange={handleChange} required style={{ 
                        width: '100%', 
                        padding: '8px',
                        borderColor: '#457B9D', 
                        borderRadius: '4px' 
                    }} />
                </label>
                <label style={{ color: '#1D3557' }}>
                    Color:
                    <input type="text" name="color" value={formData.color} onChange={handleChange} required style={{ 
                        width: '100%', 
                        padding: '8px',
                        borderColor: '#457B9D'
                    }} />
                </label>
                <label style={{ color: '#1D3557' }}>
                    Year:
                    <input type="number" name="year" value={formData.year} onChange={handleChange} required style={{ 
                        width: '100%', 
                        padding: '8px',
                        borderColor: '#457B9D'
                    }} />
                </label>
                <label style={{ color: '#1D3557' }}>
                    Model:
                    <select name="modelId" value={formData.modelId} onChange={handleChange} required style={{ 
                        width: '100%', 
                        padding: '8px',
                        borderColor: '#457B9D',
                        backgroundColor: '#F1FAEE' 
                    }}>
                        <option value="">Select a Model</option>
                        {models.map(model => (
                            <option key={model.id} value={model.id}>{model.name}</option>
                        ))}
                    </select>
                </label>
                <label style={{ color: '#1D3557' }}>
                    Sold:
                    <input type="checkbox" name="sold" checked={formData.sold} onChange={handleChange} />
                </label>
                <button type="submit" style={{ 
                    padding: '10px', 
                    backgroundColor: '#E63946', 
                    color: 'white', 
                    border: 'none', 
                    borderRadius: '4px', 
                    cursor: 'pointer' 
                }}>Add Automobile</button>
            </form>
            {message && <p style={{ color: 'green' }}>{message}</p>}
        </div>
    );
    
}

export default AddAutomobile;

