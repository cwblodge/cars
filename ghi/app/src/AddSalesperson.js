import React, { useState } from 'react';

function AddSalesperson() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const salespersonData = {
            first_name: firstName,  
            last_name: lastName,    
            employee_id: employeeId 
        };

        try {
            const response = await fetch('http://localhost:8090/api/salespeople/', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(salespersonData)
            });
            if (!response.ok) {
                throw new Error('Failed to add salesperson');
            }
            const result = await response.json();
            console.log('Salesperson added:', result);
            setFirstName('');
            setLastName('');
            setEmployeeId('');
        } catch (error) {
            console.error('Error:', error.message);
        }
    };


    return (
        <div style={{ padding: 20, maxWidth: 400, margin: "auto" }}>
            <h2 style={{ color: '#E63946' }}>Add a Salesperson</h2>
            <form onSubmit={handleSubmit} style={{ display: 'table', width: '100%' }}>
                <div style={{ display: 'table-row' }}>
                    <div style={{ display: 'table-cell', padding: 10 }}>
                        <label style={{ color: 'black' }}>First name:</label>
                    </div>
                    <div style={{ display: 'table-cell', padding: 10 }}>
                        <input
                            type="text"
                            value={firstName}
                            onChange={e => setFirstName(e.target.value)}
                            placeholder="First name..."
                            required
                            style={{ width: '100%', padding: '8px', border: '1px solid #ccc', borderRadius: '4px', color: 'black' }}
                        />
                    </div>
                </div>
                <div style={{ display: 'table-row' }}>
                    <div style={{ display: 'table-cell', padding: 10 }}>
                        <label style={{ color: 'black' }}>Last name:</label>
                    </div>
                    <div style={{ display: 'table-cell', padding: 10 }}>
                        <input
                            type="text"
                            value={lastName}
                            onChange={e => setLastName(e.target.value)}
                            placeholder="Last name..."
                            required
                            style={{ width: '100%', padding: '8px', border: '1px solid #ccc', borderRadius: '4px', color: 'black' }}
                        />
                    </div>
                </div>
                <div style={{ display: 'table-row' }}>
                    <div style={{ display: 'table-cell', padding: 10 }}>
                        <label style={{ color: 'black' }}>Employee ID:</label>
                    </div>
                    <div style={{ display: 'table-cell', padding: 10 }}>
                        <input
                            type="text"
                            value={employeeId}
                            onChange={e => setEmployeeId(e.target.value)}
                            placeholder="Employee ID..."
                            required
                            style={{ width: '100%', padding: '8px', border: '1px solid #ccc', borderRadius: '4px', color: 'black' }}
                        />
                    </div>
                </div>
                <div style={{ display: 'table-row' }}>
                    <div style={{ display: 'table-cell', padding: 10, colspan: 2 }}>
                        <button type="submit" style={{
                            width: '100%', 
                            backgroundColor: '#4CAF50', 
                            color: 'white', 
                            border: 'none', 
                            borderRadius: '4px', 
                            padding: '10px', 
                            cursor: 'pointer'
                        }}>Create</button>
                    </div>
                </div>
            </form>
        </div>
    );
}

export default AddSalesperson;



