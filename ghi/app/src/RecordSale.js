import React, { useState, useEffect } from 'react';

function RecordSale() {
  const [automobiles, setAutomobiles] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [selectedAuto, setSelectedAuto] = useState('');
  const [selectedSalesperson, setSelectedSalesperson] = useState(-1);
  const [selectedCustomer, setSelectedCustomer] = useState(-1);
  const [price, setPrice] = useState('');
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const autoResponse = await fetch('http://localhost:8100/api/automobiles/');
        const salespeopleResponse = await fetch('http://localhost:8090/api/salespeople/');
        const salesResponse = await fetch('http://localhost:8090/api/sales/');
        const customersResponse = await fetch('http://localhost:8090/api/customers/');
  
        if (!autoResponse.ok || !salespeopleResponse.ok || !salesResponse.ok || !customersResponse.ok) {
          throw new Error('Failed to fetch data');
        }
  
        const autoData = await autoResponse.json();
        const salespeopleData = await salespeopleResponse.json();
        const salesData = await salesResponse.json();
        const customersData = await customersResponse.json();
  
        if (autoData.autos && Array.isArray(autoData.autos)) {
          setAutomobiles(autoData.autos);
        } else {
          console.error("Expected an array under 'autos' key but received:", autoData);
          setAutomobiles([]); 
        }
  
        setSalespeople(salespeopleData);
        setCustomers(customersData);
      } catch (error) {
        setError('Failed to load data: ' + error.message);
        console.error('Fetch Error:', error);
      } finally {
        setIsLoading(false);
      }
    };
  
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    setIsLoading(true);

    const body = JSON.stringify({
      automobile: automobiles[selectedAuto],
      salesperson: salespeople[selectedSalesperson],
      customer: customers[selectedCustomer],
      price: price
    });
    console.log(`POST /sales body: ${body}`);
    try {
      const response = await fetch('http://localhost:8090/api/sales/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body,
      });

      if (!response.ok) {
        throw new Error('Failed to record the sale');
      }

      alert('Sale recorded successfully!');
      setSelectedAuto('');
      setSelectedSalesperson(-1);
      setSelectedCustomer(-1);
      setPrice('');
    } catch (error) {
      setError('Error: ' + error.message);
    } finally {
      setIsLoading(false);
    }
  };

  if (isLoading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>Error: {error}</p>;
  }

  return (
    <div style={{ padding: 20, maxWidth: 600, margin: "auto", color: "black" }}>
      <h2 style={{ color: '#E63946' }}>Record a Sale</h2>
      <form onSubmit={handleSubmit} style={{ display: 'table', width: '100%' }}>
        {['auto-vin', 'salesperson', 'customer', 'price'].map((field, index) => (
          <div key={field} style={{ display: 'table-row' }}>
            <div style={{ display: 'table-cell', padding: 10 }}>
              <label htmlFor={field} style={{ color: 'black' }}>{`${field.split('-').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')}`}</label>
            </div>
            <div style={{ display: 'table-cell', padding: 10 }}>
              {field === 'price' ? (
                <input 
                  id={field} 
                  type="number" 
                  value={price} 
                  onChange={e => setPrice(e.target.value)} 
                  style={{ width: '100%', padding: '8px', borderColor: '#ccc', borderRadius: '4px', color: 'black' }} // Ensuring input style is consistent
                />
              ) : (
                <select 
                  id={field} 
                  value={index === 0 ? selectedAuto : index === 1 ? selectedSalesperson : selectedCustomer} 
                  onChange={e => index === 0 ? setSelectedAuto(e.target.value) : index === 1 ? setSelectedSalesperson(e.target.value) : setSelectedCustomer(e.target.value)} 
                  style={{ width: '100%', padding: '8px', borderColor: '#ccc', borderRadius: '4px', color: 'black' }} // Ensuring select style is consistent
                >
                  <option value="">Choose a {field.split('-')[0]}...</option>
                  {(index === 0 ? automobiles : index === 1 ? salespeople : customers).map((item, idx) => (
                    <option key={idx} value={idx} style={{ color: 'black' }}>{field === 'auto-vin' ? item.vin : `${item.first_name} ${item.last_name}`}</option>
                  ))}
                </select>
              )}
            </div>
          </div>
        ))}
        <div style={{ display: 'table-row' }}>
          <div style={{ display: 'table-cell', padding: 10, colspan: 2 }}>
            <button type="submit" style={{
              width: '100%', 
              padding: '10px', 
              backgroundColor: '#4CAF50', 
              color: 'white', 
              border: 'none', 
              borderRadius: '4px', 
              cursor: 'pointer'
            }}>
              Record Sale
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}


export default RecordSale;
