import React, { useState, useEffect } from 'react';

function ListCustomers() {
  const [customers, setCustomers] = useState([]);

  useEffect(() => {
    async function fetchCustomers() {
      try {
        const response = await fetch('http://localhost:8090/api/customers/');
        if (!response.ok) {
          throw new Error('Failed to fetch customers');
        }
        const data = await response.json();
        setCustomers(data);
      } catch (error) {
        console.error('Error:', error);
      }
    }
    fetchCustomers();
  }, []);

  const generateCustomerId = (customer) => {
    return `${customer.first_name}_${customer.last_name}_${customer.address}_${customer.phone_number}`;
  };

  const handleDelete = async (customer) => {
    try {
      const response = await fetch(`http://localhost:8090/api/customers/${customer.id}`, {
        method: 'DELETE',
      });
      if (!response.ok) {
        throw new Error('Failed to delete customer');
      }
      setCustomers(customers.filter(c => c.id !== customer.id));
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div>
      <h2 style={{ color: '#E63946' }}>Customers</h2>
      <table style={{ width: '100%', borderCollapse: 'collapse' }}>
        <thead>
          <tr>
            <th style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>First Name</th>
            <th style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>Last Name</th>
            <th style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>Phone Number</th>
            <th style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>Address</th>
            <th style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>Action</th> 
          </tr>
        </thead>
        <tbody>
          {customers.map(customer => (
            <tr key={generateCustomerId(customer)}>
              <td style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>{customer.first_name}</td>
              <td style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>{customer.last_name}</td>
              <td style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>{customer.phone_number}</td>
              <td style={{ border: '1px solid #ccc', padding: '8px', color: 'black' }}>{customer.address}</td>
              <td style={{ border: '1px solid #ccc', padding: '8px' }}>
                <button onClick={() => handleDelete(customer)} style={{
                  backgroundColor: '#ff4747', 
                  color: 'white', 
                  border: 'none', 
                  borderRadius: '4px', 
                  padding: '5px 10px', 
                  cursor: 'pointer'
                }}>
                  Delete
                </button>
              </td> 
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}


export default ListCustomers;

