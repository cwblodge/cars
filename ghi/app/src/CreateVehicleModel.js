import React, { useState, useEffect } from 'react';

function CreateVehicleModel() {
    const [manufacturers, setManufacturers] = useState([]);
    const [formData, setFormData] = useState({
        name: '',
        pictureUrl: '',
        manufacturerId: ''
    });
    const [submitting, setSubmitting] = useState(false);
    const [message, setMessage] = useState('');

    useEffect(() => {
        fetch('http://localhost:8100/api/manufacturers/')
            .then(response => response.json())
            .then(data => setManufacturers(data.manufacturers))
            .catch(error => console.error('Error fetching manufacturers:', error));
    }, []);

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        setSubmitting(true);
        fetch('http://localhost:8100/api/models/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: formData.name,
                picture_url: formData.pictureUrl,
                manufacturer_id: formData.manufacturerId
            })
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            setMessage('Vehicle model created successfully!');
            setFormData({
                name: '',
                pictureUrl: '',
                manufacturerId: ''
            }); 
            console.log('Success:', data);
        })
        .catch((error) => {
            setMessage('Failed to create vehicle model.');
            console.error('Error:', error);
        })
        .finally(() => setSubmitting(false));
    };


    return (
        <div style={{ padding: '20px 0', backgroundColor: '#f5f5f5', minHeight: '100vh' }}>
            <h1 style={{ textAlign: 'center', color: '#E63946' }}>Create a Vehicle Model</h1>
            <form onSubmit={handleSubmit} style={{
                display: 'table',
                margin: '20px auto 0',
                width: '100%',
                maxWidth: '500px',
                backgroundColor: 'white',
                padding: '20px',
                boxShadow: '0 4px 8px rgba(0,0,0,0.1)',
                borderRadius: '8px'
            }}>
                <div style={{ display: 'table-row' }}>
                    <div style={{ display: 'table-cell', padding: '10px', color: 'black' }}>Model Name:</div>
                    <div style={{ display: 'table-cell', padding: '10px' }}>
                        <input
                            type="text"
                            name="name"
                            value={formData.name}
                            onChange={handleChange}
                            required
                            style={{ width: '100%', padding: '8px', border: '1px solid #ccc', borderRadius: '4px' }}
                        />
                    </div>
                </div>
                <div style={{ display: 'table-row' }}>
                    <div style={{ display: 'table-cell', padding: '10px', color: 'black' }}>Picture URL:</div>
                    <div style={{ display: 'table-cell', padding: '10px' }}>
                        <input
                            type="url"
                            name="pictureUrl"
                            value={formData.pictureUrl}
                            onChange={handleChange}
                            required
                            style={{ width: '100%', padding: '8px', border: '1px solid #ccc', borderRadius: '4px' }}
                        />
                    </div>
                </div>
                <div style={{ display: 'table-row' }}>
                    <div style={{ display: 'table-cell', padding: '10px', color: 'black' }}>Manufacturer:</div>
                    <div style={{ display: 'table-cell', padding: '10px' }}>
                        <select
                            name="manufacturerId"
                            value={formData.manufacturerId}
                            onChange={handleChange}
                            required
                            style={{ width: '100%', padding: '8px', border: '1px solid #ccc', borderRadius: '4px' }}
                        >
                            <option value="">Select a Manufacturer</option>
                            {manufacturers.map(manufacturer => (
                                <option key={manufacturer.id} value={manufacturer.id}>
                                    {manufacturer.name}
                                </option>
                            ))}
                        </select>
                    </div>
                </div>
                <div style={{ display: 'table-row' }}>
                    <div style={{ display: 'table-cell', padding: '10px', colspan: 2 }}>
                        <button type="submit" disabled={submitting} style={{
                            width: '100%',
                            padding: '10px',
                            backgroundColor: '#4CAF50', 
                            color: 'white',
                            border: 'none',
                            borderRadius: '4px',
                            cursor: 'pointer'
                        }}>
                            {submitting ? 'Creating...' : 'Create'}
                        </button>
                    </div>
                </div>
            </form>
            {message && <p style={{ textAlign: 'center', color: 'green' }}>{message}</p>}
        </div>
    );
}

export default CreateVehicleModel;
