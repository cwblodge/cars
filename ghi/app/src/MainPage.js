import React from 'react';
import { Link } from 'react-router-dom';

function MainPage() {
  return (
    <div className="container py-5">
      <div className="row justify-content-center align-items-center">
        <div className="col-lg-8 text-center">
          <h1 className="display-4 fw-bold mb-4" style={{ color: '#ff5733' }}>Welcome to CarCar</h1>
          <p className="lead mb-4" style={{ color: '#32CD32' }}>
            Your premier solution for automobile dealership management!
          </p>
          <p className="text-muted" style={{ color: '#1c1c1c' }}>
            <span style={{ color: '#0000FF' }}>CarCar provides you with all the tools you need to streamline your dealership operations, manage your inventory, and deliver exceptional customer service. </span> 
          </p>
          <div className="mt-5">
            <Link to="/models" className="btn btn-primary btn-lg" style={{ backgroundColor: '#4287f5', borderColor: '#4287f5' }}>View Vehicle Models</Link>
          </div>
          <div className="mt-3">
            <p style={{ color: '#1c1c1c' }}>Check out the <a href="https://www.formula1.com/en/racing/2024.html" target="_blank" rel="noopener noreferrer" style={{ color: '#ff5733', textDecoration: 'underline' }}>upcoming races</a> to see the latest F1 action!</p>
          </div>
          <div className="mt-5">
            <iframe 
              width="560" 
              height="315" 
              src="https://www.youtube.com/embed/Mpg6SmlKYvE" 
              title="Aston Martin Video"
              frameBorder="0" 
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
              allowFullScreen
              className="mb-3"
            ></iframe>
            <iframe 
              width="560" 
              height="315" 
              src="https://www.youtube.com/embed/tZc5ZCW2coM" 
              title="Mflickss Video"
              frameBorder="0" 
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
              allowFullScreen
            ></iframe>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;


