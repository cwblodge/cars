import React, { useState, useEffect } from 'react';

function VehicleModels() {
    const [vehicleModels, setVehicleModels] = useState([]);
    const [manufacturers, setManufacturers] = useState([]);
    const [formData, setFormData] = useState({
        name: '',
        pictureUrl: '',
        manufacturerId: ''
    });

    useEffect(() => {
        fetch('http://localhost:8100/api/models/')
            .then(response => response.json())
            .then(data => setVehicleModels(data.models))
            .catch(error => console.error('Error fetching vehicle models:', error));
    }, []);

    useEffect(() => {
        fetch('http://localhost:8100/api/manufacturers/')
            .then(response => response.json())
            .then(data => setManufacturers(data.manufacturers))
            .catch(error => console.error('Error fetching manufacturers:', error));
    }, []);

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        fetch('http://localhost:8100/api/models/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: formData.name,
                picture_url: formData.pictureUrl,
                manufacturer_id: formData.manufacturerId
            })
        })
            .then(response => response.json())
            .then(data => {
                console.log('Success:', data);
                setVehicleModels([...vehicleModels, data]); 
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    };

    return (
        <div style={{ padding: '20px', maxWidth: '1200px', margin: 'auto' }}>
            <h1>Vehicle Models</h1>
            <div style={{ display: 'grid', gridTemplateColumns: 'repeat(auto-fill, minmax(300px, 1fr))', gap: '20px' }}>
                {vehicleModels.map(model => (
                    <div key={model.id} style={{ border: '1px solid #ccc', padding: '20px', borderRadius: '8px', transition: 'box-shadow 0.3s', boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.1)' }} onMouseEnter={(e) => e.target.style.backgroundColor = '#f0f0f0'} onMouseLeave={(e) => e.target.style.backgroundColor = 'white'}>
                        <h2 style={{ fontSize: '1.2rem', marginBottom: '10px' }}>{model.name}</h2>
                        <p style={{ fontSize: '1rem', color: '#666' }}>Manufacturer: {model.manufacturer.name}</p>
                        <img src={model.picture_url} alt={model.name} style={{ width: '100%', height: 'auto', maxWidth: '280px' }} />
                    </div>
                ))}
            </div>
            <h1>Create a Vehicle Model</h1>
            <form onSubmit={handleSubmit} style={{ display: 'flex', flexDirection: 'column', gap: '10px' }}>
                <input type="text" name="name" placeholder="Model name..." value={formData.name} onChange={handleChange} style={{ padding: '10px' }} />
                <input type="text" name="pictureUrl" placeholder="Picture URL..." value={formData.pictureUrl} onChange={handleChange} style={{ padding: '10px' }} />
                <select name="manufacturerId" value={formData.manufacturerId} onChange={handleChange} style={{ padding: '10px' }}>
                    <option value="">Choose a manufacturer...</option>
                    {manufacturers.map(manufacturer => (
                        <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                    ))}
                </select>
                <button type="submit" style={{ padding: '12px', backgroundColor: '#4CAF50', color: 'white', border: 'none', borderRadius: '4px', cursor: 'pointer' }}>Create</button>
            </form>
        </div>
    );
}

export default VehicleModels;
