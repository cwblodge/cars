import React, { useState } from 'react';

function TechnicianForm() {
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    employee_id: '',
  })

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8080/api/technicians/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        first_name: '',
        last_name: '',
        employee_id: '',
      });
      alert("Technician added successfully!");
    } else {
      alert("Failed to add technician. Please check the data provided.");
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4" style={{ color: 'black' }}>
          <h1>Add a New Technician</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-group mb-3">
              <label htmlFor="first_name">First Name</label>
              <input onChange={handleFormChange} required type="text" name="first_name" id="first_name" className="form-control" value={formData.first_name} />
            </div>

            <div className="form-group mb-3">
              <label htmlFor="last_name">Last Name</label>
              <input onChange={handleFormChange} required type="text" name="last_name" id="last_name" className="form-control" value={formData.last_name} />
            </div>

            <div className="form-group mb-3">
              <label htmlFor="employee_id">Employee ID</label>
              <input onChange={handleFormChange} required type="text" name="employee_id" id="employee_id" className="form-control" value={formData.employee_id} />
            </div>
            
            <button type="submit" className="btn" style={{ backgroundColor: '#28a745', color: 'white' }}>Add Technician</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default TechnicianForm;
