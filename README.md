# CarCar
Team:
* Cosimo - Sales
* James - Service
## Design
![](<Screenshot 2024-05-09 at 5.01.59 PM.jpeg>)
## How to initiate
Fork and Clone this repository: https://gitlab.com/cwblodge/cars.git
    follow these commands to run docker volumes, images, and containers
        docker volume create beta-data
        docker compose build
        docker compose up
## API Documentation
The CarCar project provides a suite of microservices designed to facilitate comprehensive management of automotive services. This system supports CRUD operations for service appointments, technician allocations, and vehicle records, integrating various aspects of automotive maintenance and customer service management.
## URLs and Ports ^
Service - Appointments API port 8080
    (http://localhost:8080/api/appointments/)
Service - Technician API port 8080
    (http://localhost:8080/api/technicians/)
Sales - Salespeople API port 8090
    (http://localhost:8090/api/salespeople/)
Sales - Customers API port 8090
    (http://localhost:8090/api/customers/)
Sales - Sales API port 8090
    (http://localhost:8090/api/sales/)
Inventory - Automobiles API port 8100
    (http://localhost:8100/api/automobiles/)
Inventory - Manufacturer API port 8100
    (http://localhost:8100/api/manufacturers/)
Inventory - Models API port 8100
    (http://localhost:8100/api/models/)
## Service API
The CarCar Service API enables the user to interact seamlessly with data related to automotive services, technicians, and vehicles. This documentation details the API's endpoint paths, supported operations, expected data formats, and standard error handling to facilitate effective integration with the CarCar system.
## Sales API
The CarCar Sales API provides access to vehicle sales data, customer purchase records, and sales analytics. This documentation outlines the endpoint paths, supported operations, and data formats to enable effective API integration for managing and analyzing sales activities.
## Service microservice
The Technician and Appointment models are core components of a Django application within the CarCar automotive service management microservice. The Technician model stores essential data about service personnel, which is utilized for efficient management and assignment of service tasks.
The Appointment model captures detailed information about service interactions, categorizing appointments by vehicle identification number (VIN), service type, and status. This arrangement facilitates the organization and scheduling of automotive services directly through the Service microservice.
This structure enables effective management of technician assignments, supporting the microservice architecture with RESTful endpoints. These endpoints provide robust capabilities for GET, PUT, DELETE, and POST operations, allowing for dynamic data handling and integration with other systems within the automotive service ecosystem.
## Sales microservice
The Sales microservice for the CarCar project efficiently manages the entire lifecycle of vehicle sales transactions through a well-structured model system. The AutomobileVO model is dedicated to managing detailed vehicle information, focusing on tracking the Vehicle Identification Number (VIN) and whether each vehicle is sold, which aids in inventory management and sales tracking. The Salesperson model captures essential information about sales staff, including names and unique employee IDs, directly linking each sales transaction to the responsible sales personnel for performance tracking and accountability.
The Customer model records comprehensive customer details such as name, address, and phone number, supporting effective customer relationship management and transaction history tracking. Lastly, the Sale model serves as the transactional core, linking vehicles, salespersons, and customers with specific details like the sale price. Together, these models provide robust support for RESTful API endpoints, which facilitate GET, PUT, DELETE, and POST operations, thereby enabling dynamic management of sales activities, detailed reporting, and seamless integration within the broader automotive management ecosystem.
## Value Objects
Service
    Technician:
       First Name
       Last Name
       Employee ID
    AutomobileVO:
        Vin
        Sold
    Appointment
        Date Time
        Reason
        Status
        Vin
        Customer
        Technician
Sales
    Salesperson
       First Name
       Last Name
       Employee ID
    Customer
        First Name
        Last Name
        Address
        Phone Number
    Sale
        Automobile
        Salesperson
        Customer
        Price
Inventory
    Manufacturer
       Name
    Vehicle Model
       Name
       Picture URL
       Manufacturer
    Automobile
        Color
        Year
        Vin
        Sold
        Model
## CRUD
Inventory - Automobiles:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create automobile | POST | http://localhost:8100/api/automobiles/
| Get automobile detail | GET | http://localhost:8100/api/automobiles/<str:vin>/
| Update automobile | PUT | http://localhost:8100/api/automobiles/<str:vin>/
| Delete automobile | DELETE | http://localhost:8100/api/automobiles/<str:vin>/
Inventory - Manufacturer:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get manufacturer detail | GET | http://localhost:8100/api/manufacturers/<int:pk>/
| Update manufacturer | PUT | http://localhost:8100/api/manufacturers/<int:pk>/
| Delete manufacturer | DELETE | http://localhost:8100/api/manufacturers<int:pk>/
Inventory - Models:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List models | GET | http://localhost:8100/api/models/
| Create model | POST | http://localhost:8100/api/models/
| Get model detail | GET | http://localhost:8100/api/models/<int:pk>/
| Update model | PUT | http://localhost:8100/api/models/<int:pk>/
| Delete model | DELETE | http://localhost:8100/api/models/<int:pk>/
Service - Technicians:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Get technician detail | GET | http://localhost:8080/api/technicians/<int:pk>/
| Create technician | POST | http://localhost:8080/api/technicians/
| Delete technician | DELETE | http://localhost:8080/api/technicians/<int:pk>/
Service - Appointments:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List appointments | GET | http://localhost:8080/api/appointments/
| Create appointment | POST | http://localhost:8080/api/appointments/
| Delete appointment | DELETE | http://localhost:8080/api/appointments/<int:pk>/
| Update appointment (cancel) | PUT |  http://localhost:8080/api/appointments/cancel/
| Update appointment (finish) | PUT |  http://localhost:8080/api/appointments/finish/
Sales - Salespeople:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Create salesperson | POST | http://localhost:8090/api/salespeople/
| Delete salesperson | DELETE | http://localhost:8090/api/salespeople/<int:id>/
Sales - Customers:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create customer | POST | http://localhost:8090/api/customers/
| Delete customer | DELETE | http://localhost:8090/api/customers/<int:id>/
Sales - Sales:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List sales | GET | http://localhost:8090/api/sales/
| Get sale detail | GET | http://localhost:8090/api/sale/detail/<int:id>/
| Create sale | POST | http://localhost:8090/api/sales/
| Delete sale | DELETE | http://localhost:8090/api/sales/<int:id>/